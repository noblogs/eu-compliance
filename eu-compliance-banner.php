<?php
/*
* Plugin Name: Ai - Eu compliant banner
* Plugin URI:
* Description: AI - Cookie Banner
* Version: 1.0
* Author: lechuck
* Author URI: http://noblogs.org
**/

/**
 * Proper way to enqueue scripts and styles
 */
function load_banner() {
	wp_enqueue_script( 'cookiebanner', plugins_url( '/js/cookiebanner.js' , __FILE__ ), array(), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'load_banner' );

add_filter( 'script_loader_tag', function ( $tag, $handle ) {

    if ( 'cookiebanner' !== $handle )
        return $tag;
    $stringa = ' id="cookiebanner" data-moreinfo="https://noblogs.org/privacy" data-message="To comply with the EU Privacy Laws we\'re bound to inform you that some third party services used on some of our blogs (like Youtube) could use cookies" ';

    return str_replace( ' src', "$stringa src", $tag );
}, 10, 2 );

